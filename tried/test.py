# import argv from sys와 동일한 과정
import sys

script, encoding_type, error_type = sys.argv

# line 변수에 언어를 하나씩 넣어줌. 재귀함수임에 주목
def main(file, encoding_type, error_type):
	    line = file.readline()

		     if line:
			          print_line(line, encoding_type, error_type)
	        main(file, encoding_type, error_type)

#각 언어를 한번은 인코딩 하고 다시 그것을 디코딩하여 각각 프린트한다.
def print_line(text_line, encoding, error_type):
	next_lang = text_line.strip()
	raw_bytes = next_lang.encode(encoding, errors = error_type)
	cooked_string = raw_bytes.decode(encoding, errors = error_type)

	print(raw_bytes, " <====> ", cooked_string)

#파일 open
language = open("language.txt", encoding = "utf-8")

#메인함수 시작
main(language, encoding_type, error_type)
